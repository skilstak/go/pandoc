package pandoc

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	"github.com/ghodss/yaml"
	"github.com/pelletier/go-toml"
)

// Opt contains the Pandoc command line options.
type Opt struct {
	Reader      string   // reader format (from)
	Writer      string   // writer format (to)
	Standalone  bool     // include header, footer
	Template    string   // path to custom template
	OutputFile  string   // path to output file
	InputFiles  []string // paths to input files
	NoHighlight bool     // disabl syntax highlighting

	/* TODO
	TabStop               int               // number of spaces per tab
	PreserveTabs          bool              // preserve tabs instead of converting to spaces
	TableOfContents       bool              // include table of contents
	BaseHeaderLevel       int               // base header level
	Variables             map[string]string // template variables to set
	MetaData              map[string]string // metadata fields to set
	MetadataFile          string            // path to YAML metadata file
	NumberSections        bool              // number sections in LaTeX
	NumberOffset          []int             // starting number for sections
	SectionDivs           bool              // put sections in div tags in HTML
	Incremental           bool              // use incremental lists in slidy, etc.
	SelfContained         bool              // make HTML accessible offline
	HTMLQTags             bool              // use q tags in html
	HighlightStyle        string            // style to use for highlighted code
	SyntaxDefinitions     []string          // xml syntax defs to load
	TopLevelDivision      string            // default, section, chapter, part
	HTMLMathMethod        string            // plain, webtex, gladtex, mathml,mathjax, katex
	Abbreviations         string            // path to abbreviations file
	ReferenceDoc          string            // path to reference doc
	EpubSubdirectory      string            // epub subdirectory in ocf container
	EpubMetaData          string            // epub metadata
	EpubFonts             []string          // epub fonts to embed
	EpubChapterLevel      int               // header level at which to split chapters
	EpubCoverImage        string            // path to epub cover image
	TOCDepth              int               // number of levels to include in toc
	DumpArgs              bool              // output command-line arguments
	IgnoreArgs            bool              // ignore command-line arguments
	Verbosity             string            // error, warning, info (diagnostic output)
	Trace                 bool              // enable tracing
	LogFile               string            // path to json log output file
	FailIfWarnings        bool              // fail on warnings
	ReferenceLinks        bool              // use ref links writing markdown, rst
	ReferenceLocation     string            // end of block, section, document
	*/
	DPI int // dpi
	/*
		WrapText              string            // auto, none, preserve
		Columns               int               // line length in characters (runes)
		Filters               []string          // paths to (lua) filters
		EmailObfuscation      string            // none, javascript, references
		IdentifierPrefix      string            // auto-identifier prefix
		StripEmptyParagraphs  bool              // strip empty paragraphs
		IndentedCodeClasses   []string          // classes to apply to indented blocks
		DataDir               string            // path to the main data directory
		CiteMethod            string            // citeproc, natbib, biblatex
		Listings              bool              // use listings package for code bocks
		PDFEngine             string            // program for latex/html -> pdf conversion
		PDFEngineArgs         []string          // args to pdf engine
		SlideLevel            int               // header level that creates slides
		SetextHeaders         bool              // use Setext headers for markdown 1-2
		ASCII                 bool              // prefer ascii output
		DefaultImageExtension string            // default image extension
		ExtractMedia          string            // path to extract embedded media
		TrackChanges          bool              // accept or reject ms word track-changes
		FileScope             bool              // parse input files before combining
		TitlePrefix           string            // prefix for title
		CSS                   []string          // paths to css files to link to
		IpynbOutput           string            // f to use best data; nothing to omit
		IncludeBeforeBody     []string          // files to include before
		IncludeAfterbody      []string          // file to include after body
		IncludeInHeader       []string          // files to include in header
		ResourcePath          []string          // path to search for images, etc.
		RequestHeaders        map[string]string // headers for http requests
		EOL                   string            // lf, crlf, native line ending to use
		StripComments         bool              // skip html comments
	*/
}

func (o Opt) Args() []string {
	args := []string{}
	if len(o.Reader) > 0 {
		args = append(args, "--read="+o.Reader)
	}
	if len(o.Writer) > 0 {
		args = append(args, "--write="+o.Writer)
	}
	if len(o.Template) > 0 {
		args = append(args, "--template="+o.Template)
	}
	if len(o.OutputFile) > 0 {
		args = append(args, "--output="+o.OutputFile)
	}
	if o.Standalone {
		args = append(args, "--standalone")
	}
	if o.NoHighlight {
		args = append(args, "--no-highlight")
	}
	if len(o.InputFiles) > 0 {
		for _, file := range o.InputFiles {
			args = append(args, file)
		}
	}
	// TODO
	return args
}

// Command returns a string suitable for running from the shell.
func (o Opt) Command() string {
	return "pandoc\n  " + strings.Join(o.Args(), " \\\n  ")
}

// Load loads the Pandoc options structure from a structured data file. The
// type of file is detected from the suffix. YAML (".yaml"), JSON (".json"),
// and TOML (".toml") file formats are supported. JSON is the default if when
// a known suffix is not detected. Returns any error if encountered.
func (o *Opt) Load(path string) error {
	byt, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	suf := filepath.Ext(path)
	switch suf {
	case ".yaml":
		return yaml.Unmarshal(byt, o)
	case ".toml":
		return toml.Unmarshal(byt, o)
	default:
		return json.Unmarshal(byt, o)
	}
	return nil
}

// String fulfills the Stringer interface as JSON.
func (o Opt) String() string { return toJSON(o) }

func toJSON(thing interface{}) string {
	byt, err := json.MarshalIndent(thing, "", "  ")
	if err != nil {
		return fmt.Sprintf("{\"ERROR\": \"%v\"}", err)
	}
	return string(byt)
}
