package pandoc

import "os/exec"

// Command return a Cmd from os/exec but takes an Opt structure instead of
// arguments and assumes "pandoc" for the executable string.
func Command(opt *Opt) *exec.Cmd {
	return exec.Command("pandoc", opt.Args()...)
}

// Exec executes the pandoc command and returns the combined output of stdin
// and stderr exactly as would be seen from the shell. This is most useful when
// opt.InputFiles are defined as well as opt.Outputfile. In that cases any
// output from the pandoc command itself is just informational. Use Command()
// instead when more direct control and separation of the stdin and stderr
// input is required.
func Exec(opt *Opt) string {
	buf, _ := Command(opt).CombinedOutput()
	return string(buf)
}
