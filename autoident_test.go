package pandoc_test

import (
	"fmt"

	"gitlab.com/skilstak/go/pandoc"
)

func ExampleNewIdent() {

	// These are the examples from the Pandoc Markdown documentation:

	fmt.Println(pandoc.AutoIdent("Heading identifiers in HTML"))
	fmt.Println(pandoc.AutoIdent("Maître d'hôtel"))
	fmt.Println(pandoc.AutoIdent("Dogs*?--in *my* house?"))
	fmt.Println(pandoc.AutoIdent("[HTML], [S5], or [RTF]?"))
	fmt.Println(pandoc.AutoIdent("3. Applications"))
	fmt.Println(pandoc.AutoIdent("33"))
	fmt.Println(pandoc.AutoIdent("KN^3.v3"))

	// Output:
	//
	// heading-identifiers-in-html
	// maître-dhôtel
	// dogs--in-my-house
	// html-s5-or-rtf
	// applications
	//
	// kn3.v3
}
