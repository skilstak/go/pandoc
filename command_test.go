package pandoc

import "testing"

func TestExec(t *testing.T) {
	o := new(Opt)
	o.Reader = "markdown+smart"
	o.OutputFile = "index.html"
	o.InputFiles = []string{"meta.yaml", "README.md"}
	o.Standalone = true
	o.NoHighlight = true
	out := Exec(o)
	t.Log(out)
}
