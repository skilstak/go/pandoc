package pandoc

import "testing"

func TestOpt_Load_json(t *testing.T) {
	opt := new(Opt)
	err := opt.Load("testdata/opts.json")
	if err != nil {
		t.Error(err)
	}
	t.Log(opt)
	if opt.DPI != 60 {
		t.Error("failed to parse dpi from opts.json")
	}
}

func TestOpt_Load_default(t *testing.T) {
	opt := new(Opt)
	err := opt.Load("testdata/opts")
	if err != nil {
		t.Error(err)
	}
	t.Log(opt)
	if opt.DPI != 60 {
		t.Error("failed to parse dpi from opts")
	}
}

func TestOpt_Load_yaml(t *testing.T) {
	opt := new(Opt)
	err := opt.Load("testdata/opts.yaml")
	if err != nil {
		t.Error(err)
	}
	t.Log(opt)
	if opt.DPI != 60 {
		t.Error("failed to parse dpi from opts.yaml")
	}
}

func TestOpt_Load_toml(t *testing.T) {
	opt := new(Opt)
	err := opt.Load("testdata/opts.toml")
	if err != nil {
		t.Error(err)
	}
	t.Log(opt)
	if opt.DPI != 60 {
		t.Error("failed to parse dpi from opts.toml")
	}
}

func TestOpt_Args(t *testing.T) {
	o := new(Opt)
	o.Reader = "markdown+smart"
	o.OutputFile = "index.html"
	o.InputFiles = []string{"meta.yaml", "README.md"}
	o.Standalone = true
	o.NoHighlight = true
	t.Log(o.Args())
}

func TestOpt_Command(t *testing.T) {
	o := new(Opt)
	o.Reader = "markdown+smart"
	o.OutputFile = "index.html"
	o.InputFiles = []string{"meta.yaml", "README.md"}
	o.Standalone = true
	o.NoHighlight = true
	t.Log(o.Command())
}
