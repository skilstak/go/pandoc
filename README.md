# Pandoc Go Helper Package

This package currently wraps the `pandoc` executable into an `exec.Command` from the `os/exec` standard Go package to provide state and execution efficiencies including concurrent processing of multiple input files which can dramatically improve build times for particularly large projects. Channels to individual open `pandoc` processes can also be opened and maintained in order to avoid the `pandoc` executable startup overhead as well. 

Pandoc argument options can be stored and loaded from JSON, YAML, and TOML structured data files.

## Design Considerations

Here is the reasoning behind certain design decisions.

### Just Use the Pandoc Executable

Pandoc is a complex, significant Haskell application and its highly active support community is focused on maintaining it above all. It follows then that any `pandoc` integration effort into another language at least first consider the most efficient encapsulation of this command and/or library directly.

### Data Structure for Command Line Options

The data structure for the command line options is built from the [same](https://hackage.haskell.org/package/pandoc-2.7.3/docs/Text-Pandoc-App.html) Haskell implementation created for the same purpose.

## Future Plans

Eventually certain Pandoc functionality will also be ported to native Go with the dream of creating a 100% compatible Go pandoc package that follows the Haskell Pandoc API directly, including the internal Pandoc AST, readers, and writers. 

Rather than (or in addition to) supporting Lua plugins the hope is to implement a plugin VM that uses a concurrent RPC model using goroutines with channels that have loaded independent executables implemented as plugins (much like [Hashicorp's RPC Plugins](https://github.com/hashicorp/go-plugin). This would free those extending plugins to use any compiled language.

